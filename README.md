# DigitalImageProcessing

A Study Oriented Project in Digital Image Processing, which involved re-construction of an algorithm for block-based motion Estimation.

We present the implementation of existing algorithms for better understanding of block- based motion estimation in present sense. This procedure involves transforming video sequences from a multibit to a one-bit/pixel representation and then applying
conventional motion estimation search strategies. This results in substantial reductions in arithmetic and hardware complexity and reduced power consumption, while maintaining good compression performance.
Typically, the block-matching algorithm is embedded into a motion estimation (ME) search algorithm. This search can be done in many different ways, but the simplest is a full search. This is an exhaustive comparison of all blocks in a specified range, pixel by pixel.
In block motion estimation, search pattern with different shape or size has very important impact on search speed and distortion performance. In this report, we study novel algorithms using full search, diamond search and hexagon-based search pattern for fast block motion estimation. The speedup gain of the search algorithms vary according to the amount of computatons performed in them.
The ultimate aim is to gain a higher speedup in the implementation. The first step in the speedup of the ME algorithm is to reduce the number of block comparisons required.
Reductions on the order of 3-8X can be made by using various searche strategies to improve
the efficiency of the encoder while maintaining acceptable video quality.